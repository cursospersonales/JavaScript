//String
var nombre= "Martin";
console.log(nombre)

//Numerico
var num=10;
console.log(num)

//Objetos (object)
var obj={
    nombre:'Eval',
    ape:'Apell',
    tel:44
}
console.log(obj);

//Booleano
var bandera= true;
console.log(bandera, typeof bandera);

//Funcion es tipo de dato (function)

function mifuncion(){}
console.log(typeof mifuncion)

//Symbol
var sim=Symbol("mi")
console.log(typeof sim)

//Tipo de clase es funcion
class Persona{
    constructor(nombre,apellido){
            this.nombre=nombre
            this.apellido=apellido
    }
}

console.log(typeof Persona)

//Tipo undefined
var x;
console.log(x);

//Tipo null object
var y=null;
console.log(typeof y)

//==========ARRAYS (object)===========

var autos=['bmx','audi','volvo']
console.log(autos)
console.log(typeof autos)

var z= '';
console.log(z)
